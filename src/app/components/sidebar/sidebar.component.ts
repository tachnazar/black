import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
import { UserService } from 'src/app/core/user.service';



declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  {
    path: '/dashboard',
    title: 'Dashboard',
    icon: 'icon-chart-pie-36',
    class: ''
  },
  {
    path: '/testing',
    title: 'Run Test',
    icon: 'icon-paper',
    class: ''
  },
  {
    path: '/notifications',
    title: 'Notifications',
    icon: 'icon-bell-55',
    class: ''
  },

  {
    path: '/user',
    title: 'User Profile',
    icon: 'icon-single-02',
    class: ''
  },
  {
    path: '/all-results',
    title: 'My Test Results',
    icon: 'icon-single-02',
    class: ''
  }
];
export const ROUTESADMIN: RouteInfo[] = [
  {
    path: '/dashboard',
    title: 'Dashboard',
    icon: 'icon-chart-pie-36',
    class: ''
  },
  {
    path: '/testing',
    title: 'Run Test',
    icon: 'icon-paper',
    class: ''
  },
  {
    path: '/notifications',
    title: 'Notifications',
    icon: 'icon-bell-55',
    class: ''
  },

  {
    path: '/user',
    title: 'User Profile',
    icon: 'icon-single-02',
    class: ''
  },
  {
    path: '/members',
    title: 'Members',
    icon: 'icon-single-02',
    class: ''
  },
  {
    path: '/all-results',
    title: 'My Test Results',
    icon: 'icon-single-02',
    class: ''
  }
];
// import * as firebase from 'firebase/app';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  isLoggedIn: boolean;
  $user: firebase.User;
  email: string;
  displayName: any;
  avatar: any;
  age: any;
  isAdmin: any;
  menuItemsAdmin: any[];

  constructor( private userService: UserService) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.isLoggedIn = true;
        console.log(user.uid);
        this.$user = user;
        this.email = user.email;
        this.getUserData(user.uid);
      } else {
        console.log('not logged in');
      }
    });
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.menuItemsAdmin = ROUTESADMIN.filter(menuItem => menuItem);
  }
  getUserData(uid) {
    this.userService.getUser(uid).get()
    .subscribe(doc => {
      if (doc) {
        this.displayName = doc.data().displayName;
        this.avatar = doc.data().avatar;
        this.age =  doc.data().age;
        this.isAdmin = doc.data().isAdmin;
        console.log('Document data:', this.isAdmin);
      } else {
          // doc.data() will be undefined in this case
          console.log('No such document!');
      }
  });
  }
  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }
}
