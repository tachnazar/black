import { Component, OnInit, ViewChild } from '@angular/core';
import * as firebase from 'firebase/app';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { UserService } from 'src/app/core/user.service';
@Component({
  selector: 'app-all-result',
  templateUrl: './all-result.component.html',
  styleUrls: ['./all-result.component.scss']
})
export class AllResultComponent implements OnInit {
  displayedColumns: string[] = ['Score','Interval Speed', 'Date of Test', 'Gender', 'Nationality', 'Age'];
  dataSource;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  $user: firebase.User;
  uid: string;
  email: string;

  constructor(public userService: UserService) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        console.log(user.uid);
        this.$user = user;
        this.uid = user.uid;
        this.email = user.email;
      } else {
        console.log('not logged in');
      }
    });
   }

  ngOnInit() {
    this.getAllResults(this.uid);
    this.dataSource.paginator = this.paginator;
  }
  getAllResults(uid){
    var user = this.userService.getAllResults(uid).then( function(result) {
      console.log(result);
     return result;
   });
   user.then((val) => {
     this.dataSource =  new MatTableDataSource(val);
     console.log('val',this.dataSource)
     })
   }

}
