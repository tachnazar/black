import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage = '';
  user: any;

  constructor(
    public authService: AuthService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.createForm();
    // this.user = this.authService.afAuth.user;
  }
  ngOnInit(): void {
  }

  createForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required ],
      password: ['', Validators.required]
    });
  }
  resetPassword(value) {
    if (!value.email) {
      alert('Type in your email first');
    }
    this.authService.resetPasswordInit(value.email)
    .then(
      () => alert('A password reset link has been sent to your email address'),
      (rejectionReason) => alert(rejectionReason))
    .catch(e => alert('An error occurred while attempting to reset your password'));
  }

   tryFacebookLogin() {
    this.authService.doFacebookLogin()
    .then(res => {
      console.log(res);
      this.user = res;
      this.router.navigate(['/dashboard']);
    }, err => {
      console.log(err);
    });
  }


   tryGoogleLogin() {
    this.authService.doGoogleLogin()
    .then(res => {
      console.log(res);
      this.router.navigate(['/dashboard']);
    }, err => {
      console.log('there is error', err);
    });
  }

   tryLogin(value) {
    this.authService.doLogin(value)
    .then(res => {
      console.log(res);
      this.router.navigate(['/dashboard']);
    }, err => {
      console.log(err);
      this.errorMessage = err.message;
    });
  }

}
