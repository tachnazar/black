import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/core/user.service';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {
  users;
  displayedColumns: string[] = ['Picture', 'Name', 'Gender', 'Nationality', 'Age'];
  dataSource;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public userService: UserService) {
   
   }

  ngOnInit() {
    this.getAllUsers();
    this.dataSource.paginator = this.paginator;
  }
  getAllUsers(){
   var user = this.userService.getMarker().then( function(result) {
     console.log(result);
    return result;
  });
  user.then((val) => {
    this.dataSource =  new MatTableDataSource(val);
    console.log('val',this.dataSource)
    })
  }
  
  
}
