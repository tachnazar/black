import { Component, OnInit } from '@angular/core';
import { TestingServiceService } from 'src/app/core/testing-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  point: number;

  constructor(private testing: TestingServiceService,private router: Router) { }

  ngOnInit() {
    this.getResult();
  }
  getResult() {
    this.point = this.testing.getResult();
    console.log('point in result page', this.point);
  }
  goToTest(){
    this.router.navigate(['/testing']);
  }
goToDash(){
  this.router.navigate(['/dashboard']);
}
}
