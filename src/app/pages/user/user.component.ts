import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { UserService } from 'src/app/core/user.service';
import { AuthService } from 'src/app/core/auth.service';
import * as firebase from 'firebase/app';
import { AppUser } from 'src/app/core/app-user';
import { Observable } from 'rxjs';
import { TestingServiceService } from 'src/app/core/testing-service.service';

@Component({
  selector: 'app-user',
  templateUrl: 'user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  exampleForm: FormGroup;
  $user: any;
  displayName: string;
  avatar: string;
  age: number;
  email: string;
  desctiption: string;
  gender: string;
  nationality: string;
  uid: string;
  constructor(
    public userService: UserService,
    public authService: AuthService,
    public testingService: TestingServiceService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder) {
      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          console.log(user.uid);
          this.$user = user;
          this.uid = user.uid;
          this.email = user.email;
          this.getUserData(user.uid);
        } else {
          console.log('not logged in');
        }
      });
    }

  ngOnInit() {
    this.createForm();
  }
  getUserData(uid) {
    this.userService.getUser(uid).get()
    .subscribe(doc => {
      if (doc) {
        this.displayName = doc.data().displayName;
        this.avatar = doc.data().avatar;
        this.age =  doc.data().age;
        this.gender = doc.data().gender;
        this.nationality = doc.data().nationality;
        console.log('Document data:', this.$user, doc.data());
      } else {
          // doc.data() will be undefined in this case
          console.log('No such document!');
      }
  });
  }
  createForm() {
    this.exampleForm = this.fb.group({
      displayName: [''],
      age: [''],
      gender: [''],
      nationality: [''],
      email: [{value: this.email, disabled: true}]
    });
  }
  save(value) {
    console.log('update', value );
    value.avatar = this.avatar;
    
    if(value.age == ""){
      value.age = this.age;
    }
    if(value.displayName == ""){
      value.displayName = this.displayName
    }
    if(value.gender == ""){
      value.gender = this.gender
    }
    if(value.nationality == ""){
      value.nationality = this.nationality
    }
      console.log('save called', value);
      this.userService.updateUser(this.uid, value)
      .then(res => {
        console.log('updated user', res);

        location.reload();
      }, err => console.log(err));
    
  }
}
