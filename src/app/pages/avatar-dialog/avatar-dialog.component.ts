import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-avatar-dialog',
  templateUrl: './avatar-dialog.component.html',
  styleUrls: ['./avatar-dialog.component.scss']
})
export class AvatarDialogComponent implements OnInit {

  avatars = ['https://i.pravatar.cc/150?img=68','https://i.pravatar.cc/150?img=69','https://i.pravatar.cc/150?img=70','https://i.pravatar.cc/150?img=12','https://i.pravatar.cc/150?img=9', 'https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg']
 
  constructor(
    public dialogRef: MatDialogRef<AvatarDialogComponent>
  ) { }

  ngOnInit() {
  }

  

  close(avatar){
    this.dialogRef.close(avatar);
  }

}
