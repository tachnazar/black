import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth.service';
import { UserService } from 'src/app/core/user.service';
import { MatDialog } from '@angular/material';
import { AvatarDialogComponent } from '../avatar-dialog/avatar-dialog.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  exampleForm: FormGroup;
  errorMessage = '';
  avatarLink = 'https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg';

  validation_messages = {
   displayName: [
     { type: 'required', message: 'Name is required.' }
   ],
   age: [
     { type: 'required', message: 'Age is required.' }
   ],
   gender: [
    { type: 'required', message: 'Gender is required'}
  ],
    nationality: [
     { type: 'required', message: 'Nationality is required'}
   ],
   email: [
    { type: 'required', message: 'email is required.' },
  ],
  password: [
    { type: 'required', message: 'password is required.' },
  ]
 };
  constructor(
    public authService: AuthService,
    public userService: UserService,
    private router: Router,
    private fb: FormBuilder,
    public dialog: MatDialog,
  ) {
   }

   ngOnInit(): void {
    this.createForm();
  }
   createForm() {
     this.exampleForm = this.fb.group({
       displayName: ['', Validators.required],
       age: ['', Validators.required],
       gender: ['', Validators.required],
       nationality: ['', Validators.required],
       email: ['', Validators.required ],
       password: ['', Validators.required]
     });
   }
   openDialog() {
    const dialogRef = this.dialog.open(AvatarDialogComponent, {
      height: '400px',
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.avatarLink = result;
      }
    });
  }
  resetFields() {
    this.avatarLink = 'https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg';
    this.exampleForm = this.fb.group({
      displayName: new FormControl('', Validators.required),
      age: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      nationality: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  onSubmit(value) {
    this.userService.createUser(value, this.avatarLink)
    .then(
      res => {
        this.resetFields();
        this.router.navigate(['/dashboard']);
        console.log('user created', res);
      }, err => {
        console.log(err);
        this.errorMessage = err.message;
      });
  }

   tryFacebookLogin() {
     this.authService.doFacebookLogin()
     .then(res => {
       this.router.navigate(['/dashboard']);
     }, err => console.log(err)
     );
   }

   tryGoogleLogin() {
     this.authService.doGoogleLogin()
     .then(res => {
       this.router.navigate(['/dashboard']);
     }, err => console.log(err)
     );
   }

}
