import { Component, OnInit, ViewChild } from '@angular/core';
import { IImage } from 'src/app/modules/slideshow/IImage';
import { TestingServiceService } from 'src/app/core/testing-service.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as firebase from 'firebase/app';


@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.scss']
})
export class TestingComponent implements OnInit {
  
  getUserData(uid: string) {
    throw new Error("Method not implemented.");
  }
  exampleForm: FormGroup;
  validation_messages = {
    speed: [
      { type: 'required', message: 'This field is required and must be a number' },
    ]
  };
  customImageUrls = [
    { url: 'assets/letters/a.png', name: 'A', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/b.png', name: 'B', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/c.png', name: 'C', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/d.png', name: 'D', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/e.png', name: 'E', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/f.png', name: 'F', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/g.png', name: 'G', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/h.png', name: 'H', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/i.png', name: 'I', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/j.png', name: 'J', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/k.png', name: 'K', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/l.png', name: 'L', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/m.png', name: 'M', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/n.png', name: 'N', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/o.png', name: 'O', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/p.png', name: 'P', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/q.png', name: 'Q', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/r.png', name: 'R', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/s.png', name: 'S', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/t.png', name: 'T', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/u.png', name: 'U', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/v.png', name: 'V', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/w.png', name: 'W', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/x.png', name: 'X', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/y.png', name: 'Y', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/letters/z.png', name: 'Z', backgroundSize: 'contain', backgroundPosition: 'center' },
  ];
  customNumberUrls = [
    { url: 'assets/numbers/1.png', name: '1', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/numbers/2.png', name: '2', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/numbers/3.png', name: '3', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/numbers/4.png', name: '4', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/numbers/5.png', name: '5', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/numbers/6.png', name: '6', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/numbers/7.png', name: '7', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/numbers/8.png', name: '8', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/numbers/9.png', name: '9', backgroundSize: 'contain', backgroundPosition: 'center' },
    { url: 'assets/numbers/10.png', name: '10', backgroundSize: 'contain', backgroundPosition: 'center' }
  ];
  imageUrls: (string | IImage)[] = this.customImageUrls;
  height = '400px';
  minHeight: string;
  arrowSize = '30px';
  showArrows = false;
  disableSwiping = false;
  autoPlay = false;
  autoPlayInterval = 150;
  stopAutoPlayOnSlide = false;
  debug = false;
  backgroundSize = 'cover';
  backgroundPosition = 'center center';
  backgroundRepeat = 'no-repeat';
  showDots = false;
  dotColor = '#FFF';
  showCaptions = false;
  captionColor = '#FFF';
  captionBackground = 'rgba(0, 0, 0, .35)';
  lazyLoad = this.imageUrls.length > 1;
  hideOnNoSlides = false;
  width = '100%';
  fullscreen = false;
  enableZoom = false;
  enablePan = false;
  beginTestClick = true;
  setupClosed = true;
  instruction = false;
  arrayForTarget = this.customNumberUrls;
  $user: firebase.User;
  uid: string;
  email: string;
  // arrayEx = 123;
  constructor(public testingService: TestingServiceService,private fb: FormBuilder) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        console.log(user.uid);
        this.$user = user;
        this.uid = user.uid;
        this.email = user.email;
      } else {
        console.log('not logged in');
      }
    });
    this.createForm(); 
    var number = this.testingService.randomIntFromInterval(0,'hey');
    console.log(number);
  }

  ngOnInit() {
  }
  public target1 = [];
  public target2 = [];
  beginTest() {
      this.beginTestClick = false;
      this.setupClosed = true;
      this.autoPlay = true;
      this.testingService.arrayForTest(this.arrayForTarget, this.customImageUrls);
      console.log('slideshow array', this.imageUrls);
  }
  createForm() {
    this.exampleForm = this.fb.group({
      speed: ['', Validators.required]
    });
  }
  setSpeed(speed){
    
    this.autoPlayInterval = speed;
    console.log('speed', speed, this.autoPlayInterval);
    this.testingService.setSpeedandId(speed, this.uid);
  }
  next(){
    this.setupClosed = false;
    this.instruction = true;
  }
  
  
};
