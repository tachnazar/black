import { Component, OnInit, Input } from '@angular/core';
import { TestingComponent } from '../testing/testing.component';
import { TestingServiceService } from 'src/app/core/testing-service.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
  target1;
  target2;
  exampleForm: FormGroup;
  validation_messages = {
    answer1: [
      { type: 'required', message: 'Answer is required.' },
    ],
    answer2: [
     { type: 'required', message: 'Answer is required.' },
   ]
  };
  constructor(private router: Router, private fb: FormBuilder, public testingService: TestingServiceService) {
    this.createForm();
   }

  ngOnInit() {
    this.getTargets();
  }
  createForm() {
    this.exampleForm = this.fb.group({
      answer1: ['', Validators.required],
      answer2: ['', Validators.required ]
    });
  }
  resetFields() {
    this.exampleForm = this.fb.group({
      answer1: new FormControl('', Validators.required),
      answer2: new FormControl('', Validators.required)
    });
  }
  getTargets(){
    this.target1 = this.testingService.getTarget1().name;
    this.target2 = this.testingService.getTarget2().name;
    console.log('targets',this.target1, this.target2);
  }
  onSubmit(value) {
    this.testingService.checkAnswer(value);
    // .then(
    //   res => {
    //     this.resetFields();
    //     this.router.navigate(['/result']);
    //     console.log('answer submitted', res);
    //   }
    // );
  }
}
