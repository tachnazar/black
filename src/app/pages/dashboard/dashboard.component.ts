import { Component, OnInit } from "@angular/core";
import * as Chart from 'chart.js';
import { UserService } from 'src/app/core/user.service';
import { map, flatMap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';

interface DocWithId {
  id: string;
}
@Component({
  selector: "app-dashboard",
  templateUrl: "dashboard.component.html"
})
export class DashboardComponent implements OnInit {
  
  public canvas : any;
  public ctx;
  public datasets: any;
  public data: any;
  public myChartData;
  public clicked: boolean = true;
  public clicked1: boolean = false;
  public clicked2: boolean = false;
  dataSource: any;
  userList: any;
  datas: any;
  allTestResults = [];
  allTestResultsByAge = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  highestScoreByAge   = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  allTestResultsByCountry = [];
  highestScoreByCountry = [];
  userId: any;
  myTestResults = [];
  constructor(public userService: UserService,public afs: AngularFirestore,) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        console.log(user.uid);
        this.userId = user.uid;
      } else {
        console.log('not logged in');
      }
    });
  }

  ngOnInit() {
    var gradientChartOptionsConfigurationWithTooltipBlue: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 60,
            suggestedMax: 125,
            padding: 20,
            fontColor: "#2380f7"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#2380f7"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipPurple: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 60,
            suggestedMax: 125,
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(225,78,202,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipRed: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 0,
            suggestedMax: 100,
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(233,32,16,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipOrange: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 50,
            suggestedMax: 110,
            padding: 20,
            fontColor: "#ff8a76"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(220,53,69,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#ff8a76"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipGreen: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 0,
            suggestedMax: 100,
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(0,242,195,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }]
      }
    };


    var gradientBarChartConfiguration: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{

          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 0,
            suggestedMax: 200,
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }],

        xAxes: [{

          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }]
      }
    };

    this.getDocumentsWithSubcollection("users","result").subscribe(res => {
      console.log('all', res)
      res.forEach(element => {
        if(element.result && element.result.length > 0){
          console.log('has result');
          element.result.forEach(result =>{
            result['result'] = result['score'] - (result['speed']/10);
            if(result['result'] < 0 ){
              result['result'] = 0;
            }
            this.allTestResults.push(result);
          })
        }
        if(element.id == this.userId){
          if(element.result && element.result.length > 0){
            console.log('has result');
            element.result.forEach(result =>{
              result['result'] = result['score'] - (result['speed']/10);
              if(result['result'] < 0 ){
                result['result'] = 0;
              }
              this.myTestResults.push(result['result']);
            })
          }
        }
      });
      console.log('my test result', this.myTestResults)
      for(var j = 0; j < this.allTestResults.length; j++){
       for(var i = 0; i < this.allTestResultsByAge.length; i++){
          if(this.allTestResults[j].age == i){
            this.allTestResultsByAge[this.allTestResults[j].age] +=  1;
          }
        }
      }
      for(var j = 0; j < this.allTestResults.length; j++){
        for(var i = 0; i < this.highestScoreByAge.length; i++){
           if(this.allTestResults[j].age == i && this.allTestResults[j].result > this.highestScoreByAge[i]){
             this.highestScoreByAge[i] =  this.allTestResults[j].result;
           }
         }
       }
       this.allTestResultsByCountry['brazil'] = 0; 
       this.allTestResultsByCountry['china'] = 0;
       this.allTestResultsByCountry['england']= 0;
       this.allTestResultsByCountry['egypt']= 0; 
       this.allTestResultsByCountry['india']= 0;
       this.allTestResultsByCountry['japan']= 0;
       this.allTestResultsByCountry['malaysia']= 0;
       this.allTestResultsByCountry['russia']= 0;
       this.allTestResultsByCountry['south africa']= 0; 
       this.allTestResultsByCountry['turkey']= 0;
       this.allTestResultsByCountry['turkmenistan']= 0;
       this.allTestResultsByCountry['thailand']= 0;
       this.allTestResultsByCountry['usa']= 0;
       this.highestScoreByCountry['brazil'] = { age: 0,  gender: "unknown", result: 0}; 
       this.highestScoreByCountry['china']= { age: 0,  gender: "unknown", result: 0};
       this.highestScoreByCountry['england']= { age: 0,  gender: "unknown", result: 0};
       this.highestScoreByCountry['egypt']= { age: 0,  gender: "unknown", result: 0}; 
       this.highestScoreByCountry['india']= { age: 0,  gender: "unknown", result: 0};
       this.highestScoreByCountry['japan']= { age: 0,  gender: "unknown", result: 0};
       this.highestScoreByCountry['malaysia']= { age: 0,  gender: "unknown", result: 0};
       this.highestScoreByCountry['russia']= { age: 0,  gender: "unknown", result: 0};
       this.highestScoreByCountry['south africa']= { age: 0,  gender: "unknown", result: 0}; 
       this.highestScoreByCountry['turkey']= { age: 0,  gender: "unknown", result: 0};
       this.highestScoreByCountry['turkmenistan']= { age: 0,  gender: "unknown", result: 0};
       this.highestScoreByCountry['thailand']= { age: 0,  gender: "unknown", result: 0};
       this.highestScoreByCountry['usa']= { age: 0,  gender: "unknown", result: 0};
       
       
       for(var j = 0; j < this.allTestResults.length; j++){
         if(this.allTestResults[j].nationality == 'angolan' || this.allTestResults[j].nationality == 'turkmen'){
           this.allTestResultsByCountry['turkmenistan'] += 1;
           if(this.highestScoreByCountry['turkmenistan'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['turkmenistan'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'thai'){
          this.allTestResultsByCountry['thailand'] += 1;
          if(this.highestScoreByCountry['thailand'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['thailand'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'chinese'){
          this.allTestResultsByCountry['china'] += 1;
          if(this.highestScoreByCountry['china'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['china'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'american'){
          this.allTestResultsByCountry['usa'] += 1;
          if(this.highestScoreByCountry['usa'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['usa'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'british'){
          this.allTestResultsByCountry['england'] += 1;
          if(this.highestScoreByCountry['england'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['england'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'brazilian'){
          this.allTestResultsByCountry['brazil'] += 1;
          if(this.highestScoreByCountry['brazil'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['brazil'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'indian'){
          this.allTestResultsByCountry['india'] += 1;
          if(this.highestScoreByCountry['india'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['india'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'japanese'){
          this.allTestResultsByCountry['japan'] += 1;
          if(this.highestScoreByCountry['japan'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['japan'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'russian'){
          this.allTestResultsByCountry['russia'] += 1;
          if(this.highestScoreByCountry['russia'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['russia'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'turkish'){
          this.allTestResultsByCountry['turkey'] += 1;
          if(this.highestScoreByCountry['turkey'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['turkey'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'malaysian'){
          this.allTestResultsByCountry['malaysia'] += 1;
          if(this.highestScoreByCountry['malaysia'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['malaysia'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'egyptian'){
          this.allTestResultsByCountry['egypt'] += 1;
          if(this.highestScoreByCountry['egypt'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['egypt'] = this.allTestResults[j];
           }
         }else
         if(this.allTestResults[j].nationality == 'south african'){
          this.allTestResultsByCountry['south africa'] += 1;
          if(this.highestScoreByCountry['south africa'].result < this.allTestResults[j].result){
            this.highestScoreByCountry['south africa'] = this.allTestResults[j];
           }
         }

       }
      console.log('all test result by country', this.allTestResultsByCountry, this.highestScoreByCountry['turkey'])
      // this.canvas = document.getElementById("chartLineRed");
      // this.ctx = this.canvas.getContext("2d");

    // var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

    // gradientStroke.addColorStop(1, 'rgba(233,32,16,0.2)');
    // gradientStroke.addColorStop(0.4, 'rgba(233,32,16,0.0)');
    // gradientStroke.addColorStop(0, 'rgba(233,32,16,0)'); //red colors

    // var data = {
    //   labels: ['JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
    //   datasets: [{
    //     label: "Data",
    //     fill: true,
    //     backgroundColor: gradientStroke,
    //     borderColor: '#ec250d',
    //     borderWidth: 2,
    //     borderDash: [],
    //     borderDashOffset: 0.0,
    //     pointBackgroundColor: '#ec250d',
    //     pointBorderColor: 'rgba(255,255,255,0)',
    //     pointHoverBackgroundColor: '#ec250d',
    //     pointBorderWidth: 20,
    //     pointHoverRadius: 4,
    //     pointHoverBorderWidth: 15,
    //     pointRadius: 4,
    //     data: [80, 100, 70, 80, 120, 80],
    //   }]
    // };

    // var myChart = new Chart(this.ctx, {
    //   type: 'line',
    //   data: data,
    //   options: gradientChartOptionsConfigurationWithTooltipRed
    // });


    this.canvas = document.getElementById("chartLineGreen");
    this.ctx = this.canvas.getContext("2d");


    var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(66,134,121,0.15)');
    gradientStroke.addColorStop(0.4, 'rgba(66,134,121,0.0)'); //green colors
    gradientStroke.addColorStop(0, 'rgba(66,134,121,0)'); //green colors

    var data = {
      labels: ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30',
               '31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60'],
      datasets: [{
        label: "My personel progress",
        fill: true,
        backgroundColor: gradientStroke,
        borderColor: '#00d6b4',
        borderWidth: 2,
        borderDash: [],
        borderDashOffset: 0.0,
        pointBackgroundColor: '#00d6b4',
        pointBorderColor: 'rgba(255,255,255,0)',
        pointHoverBackgroundColor: '#00d6b4',
        pointBorderWidth: 20,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 15,
        pointRadius: 4,
        data: this.myTestResults
      }]
    };

    var myChart = new Chart(this.ctx, {
      type: 'line',
      data: data,
      options: gradientChartOptionsConfigurationWithTooltipGreen

    });


    console.log(this.allTestResultsByAge[23]);
    var chart_labels = ['15', '16', '17', '18', '19', '20', '21','22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33','34'];
    this.datasets = [
      [this.allTestResultsByAge[15],this.allTestResultsByAge[16],this.allTestResultsByAge[17],this.allTestResultsByAge[18],this.allTestResultsByAge[19],
      this.allTestResultsByAge[20],this.allTestResultsByAge[21],this.allTestResultsByAge[22],this.allTestResultsByAge[23],this.allTestResultsByAge[24],
      this.allTestResultsByAge[25],this.allTestResultsByAge[26],this.allTestResultsByAge[27],this.allTestResultsByAge[28],this.allTestResultsByAge[29],
      this.allTestResultsByAge[30],this.allTestResultsByAge[31],this.allTestResultsByAge[32],this.allTestResultsByAge[33],this.allTestResultsByAge[34]
      ],
      [this.highestScoreByAge[15],this.highestScoreByAge[16], this.highestScoreByAge[17], this.highestScoreByAge[18], this.highestScoreByAge[19],
      this.highestScoreByAge[20],this.highestScoreByAge[21], this.highestScoreByAge[22], this.highestScoreByAge[23], this.highestScoreByAge[24],
      this.highestScoreByAge[25],this.highestScoreByAge[26], this.highestScoreByAge[27], this.highestScoreByAge[28], this.highestScoreByAge[29],
      this.highestScoreByAge[30],this.highestScoreByAge[31], this.highestScoreByAge[32], this.highestScoreByAge[33], this.highestScoreByAge[34],
      ],
      [60, 80, 65, 130, 80, 105, 90, 130, 70, 115, 60, 130]
    ];
    this.data = this.datasets[0];



    this.canvas = document.getElementById("chartBig1");
    this.ctx = this.canvas.getContext("2d");

    var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(233,32,16,0.2)');
    gradientStroke.addColorStop(0.4, 'rgba(233,32,16,0.0)');
    gradientStroke.addColorStop(0, 'rgba(233,32,16,0)'); //red colors

    var config = {
      type: 'line',
      data: {
        labels: chart_labels,
        datasets: [{
          label: "Data",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: '#ec250d',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: '#ec250d',
          pointBorderColor: 'rgba(255,255,255,0)',
          pointHoverBackgroundColor: '#ec250d',
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: this.data,
        }]
      },
      options: gradientChartOptionsConfigurationWithTooltipRed
    };
    this.myChartData = new Chart(this.ctx, config);


    this.canvas = document.getElementById("CountryChart");
    this.ctx  = this.canvas.getContext("2d");
    var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(29,140,248,0.2)');
    gradientStroke.addColorStop(0.4, 'rgba(29,140,248,0.0)');
    gradientStroke.addColorStop(0, 'rgba(29,140,248,0)'); //blue colors


    var myChart = new Chart(this.ctx, {
      type: 'bar',
      data: {
        labels: ['BRAZIL', 'CHINA', 'ENGLAND', 'EGYPT', 'INDIA', 'JAPAN', 'MALAYSIA', 'RUSSIA', 'SOUTH AFRICA', 'TURKEY', 'TURKMENISTAN', 'THAILAND', 'USA'],
        datasets: [{
          label: "Data",
          fill: true,
          backgroundColor: gradientStroke,
          hoverBackgroundColor: gradientStroke,
          borderColor: '#1f8ef1',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: [this.allTestResultsByCountry['brazil'], this.allTestResultsByCountry['china'], this.allTestResultsByCountry['england'], this.allTestResultsByCountry['egypt'], 
                 this.allTestResultsByCountry['india'], this.allTestResultsByCountry['japan'],this.allTestResultsByCountry['malaysia'],this.allTestResultsByCountry['russia'],
                 this.allTestResultsByCountry['south africa'], this.allTestResultsByCountry['turkey'],this.allTestResultsByCountry['turkmenistan'],this.allTestResultsByCountry['thailand'],
                 this.allTestResultsByCountry['usa']],
        }]
      },
      options: gradientBarChartConfiguration
    });
    });
    
  }
  public updateOptions() {
    this.myChartData.data.datasets[0].data = this.data;
    this.myChartData.update();
  }
  public getAllResults(){
    var user = this.userService.getResults().then( function(result) {
      return result;
    });
    user.then((val) => {
      this.userList =  val;
      console.log('val',this.userList)
      })
  }

convertSnapshots<T>(snaps) {
  return <T[]>snaps.map(snap => {
    return {
      id: snap.payload.doc.id,
      ...snap.payload.doc.data()
    };

  });
  
}

getDocumentsWithSubcollection<T extends DocWithId>(
    collection: string,
    subCollection: string
  ) {
    return this.afs
      .collection(collection)
      .snapshotChanges()
      .pipe(
        map(this.convertSnapshots),
        map((documents: T[]) =>
          documents.map(document => {
            return this.afs
             .collection(`${collection}/${document.id}/${subCollection}`)
              .snapshotChanges()
              .pipe(
                map(this.convertSnapshots),
                map(subdocuments =>
                  Object.assign(document, { [subCollection]: subdocuments }),
                )
              );
          })
        ),
        flatMap(combined => combineLatest(combined))
      );
  }
}
