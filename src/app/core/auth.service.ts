import { Injectable } from "@angular/core";
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { UserService } from './user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AppUser } from './app-user';
import { config } from './app.config';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';


@Injectable()
export class AuthService {
  // // user$: Observable<firebase.User>;
  // user$: Observable<AppUser>;
  isLoggedIn = false;
  constructor(
   private db: AngularFirestore,
   public afAuth: AngularFireAuth,
   private userService: UserService,
   private route: ActivatedRoute,
   private router: Router
 ) {
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      const displayName = user.displayName;
      const email = user.email;
      const emailVerified = user.emailVerified;
      const isAnonymous = user.isAnonymous;
      const uid = user.uid;
      this.isLoggedIn = true;
    }
    });
  }
  doFacebookLogin(){
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.FacebookAuthProvider();
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      })
    })
  }

  async doGoogleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    console.log('logged in', credential.user, credential);
    return this.updateUserData(credential.user);
      // provider.addScope('profile');
      // provider.addScope('email');
      // this.afAuth.auth
      // .signInWithPopup(provider)
      // .then(res => {
      //   resolve(res);
      // }, err => {
      //   console.log(err);
      //   reject(err);
      // })
  }

  updateUserData(user) {
    const userRef: AngularFirestoreDocument<AppUser> = this.db.doc(`users/${user.uid}`);
    const data = {
      uid: user.uid,
      email: user.email,
      age: null,
      avatar: user.photoURL,
      displayName: user.displayName,
      gender: null,
      isAdmin: false,
      nationality: null
    };
    return userRef.set(data, {merge: true});
  }


  doLogin(value){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
      .then(res => {
        resolve(res);
        console.log('login respond',res);
      }, err => reject(err));
    });
  }

    async doLogout() {
      firebase.auth().signOut();
      console.log('logged out');
    }
  /** 
   * Initiate the password reset process for this user 
   * @param email email of the user 
   */ 
  resetPasswordInit(email: string) {
    return this.afAuth.auth.sendPasswordResetEmail(
      email,
      { url: 'http://localhost:4200/auth' });
    }
  // get appUser$(): Observable<AppUser> {
  //   return this.user$.pipe(
  //     switchMap(user => {
  //       if (user) {return this.userService.getUser(user.uid); }

  //       return of(null);
  //     })
  //   );
  // }


}
