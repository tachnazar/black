import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { ScrollStrategyOptions } from '@angular/cdk/overlay';
import * as firebase from 'firebase/app';
import { UserService } from './user.service';


@Injectable({
  providedIn: 'root'
})
export class TestingServiceService {
 
  target1: any;
  target2: any;
  answer1: any;
  answer2: any;
  points: number;
  speed: any;
  uid: any;
  userData: any;
  $user: firebase.User;
  email: string;
  displayName: any;
  avatar: any;
  age: any;
  gender: any;
  nationality: any;
  arrayEx = [] 
  constructor(public db: AngularFirestore,public afAuth: AngularFireAuth,private router: Router,public userService: UserService,) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        console.log(user.uid);
        this.$user = user;
        this.uid = user.uid;
        this.email = user.email;
        this.getUserData(user.uid);
      } else {
        console.log('not logged in');
      }
    });
    
   }
   getUserData(uid) {
    this.userService.getUser(uid).get()
    .subscribe(doc => {
      if (doc) {
        this.displayName = doc.data().displayName;
        this.avatar = doc.data().avatar;
        this.age =  doc.data().age;
        this.gender = doc.data().gender;
        this.nationality = doc.data().nationality;
        console.log('Document data:', this.$user, doc.data());
      } else {
          // doc.data() will be undefined in this case
          console.log('No such document!');
      }
  });
  }
  arrayForTest(arrayForTarget, customImageUrls){
    this.setTarget(arrayForTarget);
    this.getTarget1();
    var array = this.shuffle(customImageUrls)
    array.splice(this.randomIntFromInterval(8, 16), 0, this.target1);
    array.splice(this.randomIntFromInterval(16, 24), 0, this.target2);
    console.log('two targets',this.target1,this.target2)
    console.log('array for test', array);
  };
  randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
  setTarget(array){
    console.log("Full array", array);
    var shuffledArray = this.shuffle(array);
    this.target1 = shuffledArray[Math.floor(Math.random()*shuffledArray.length)];
    console.log("target1", this.target1);
    var index = shuffledArray.indexOf(this.target1);
    if(index>-1){
      shuffledArray.splice(index,1);
    }
    this.target2 = shuffledArray[Math.floor(Math.random()*shuffledArray.length)];
    console.log("target2", this.target2);
  };

  getTarget1() {
    console.log('target 1',this.target1)
    return this.target1;
  };
  getTarget2(){
    console.log('target 1',this.target1)
    return this.target2;
  }
  setSpeedandId(speed, id) {
    this.speed = speed;
    this.uid = id;
  }
  checkAnswer(value) {
    this.answer1 = value.answer1;
    this.answer2 = value.answer2;
    console.log('answers',this.answer1, this.answer2, this.target1, this.target2);
    if(this.answer1 == parseInt(this.target1.name) && this.answer2 == parseInt(this.target2.name)){
      this.points = 100;
    }else if(this.answer1 == parseInt(this.target1.name)){
      this.points = 50;
    }else if(this.answer2 == parseInt(this.target2.name)){
      this.points = 50;
    }else{
      this.points = 0;
    }
    this.saveResult(this.uid, this.points, this.speed).then(function() {
      console.log("Document successfully written!");
    })
    .catch(function(error) {
      console.error("Error writing document: ", error);
    });
    console.log('points', this.points, this.speed);
    return this.router.navigate(['/result']);
  }
  getResult(){
    console.log('result', this.points)
    return this.points;
  }
  
  shuffle(array) {
    if(array.length == 0){
      return console.error('array cant be empty');
    }else{
    var currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }
  };
  saveResult(userKey, scoreOfResult, speedOfResult) {
    return this.db.collection('users').doc(userKey).collection('result').add({
      score: scoreOfResult,
      speed: speedOfResult,
      // age: this.age,
      // gender: this.gender,
      // nationality: this.nationality,
      date: firebase.firestore.FieldValue.serverTimestamp()
    });
  }
}
