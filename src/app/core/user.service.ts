import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from './auth.service';
import { map, flatMap } from 'rxjs/operators';

import { combineLatest } from 'rxjs';
interface DocWithId {

  id: string;

}

@Injectable()
export class UserService {
  public uid: string;
  constructor(
   public db: AngularFirestore,
   public afAuth: AngularFireAuth
 ) {
 }


  getCurrentUser() {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          resolve(user);
        } else {
          reject('No user logged in');
        }
      });
    });
  }

  updateCurrentUser(value) {
    return new Promise<any>((resolve, reject) => {
      const user = firebase.auth().currentUser;
      user.updateProfile({
        displayName: value.displayName,
        // age: value.age,
        // gender: value.gender,
        // nationality: value.nationality,
        photoURL: user.photoURL
      }).then(res => {
        resolve(res);
      }, err => reject(err));
    });
  }
  getAvatars() {
    return this.db.collection('/avatar').valueChanges();
  }

  getUser(userKey) {
    return this.db.collection('users').doc(userKey);
  }

  updateUser(userKey, value) {
    value.nameToSearch = value.displayName.toLowerCase();
    return this.db.collection('users').doc(userKey).set(value);
  }

  deleteUser(userKey) {
    return this.db.collection('users').doc(userKey).delete();
  }

  getUsers() {
    
    return this.db.collection('users').snapshotChanges();
  }
  async getMarker() {
    const snapshot = await firebase.firestore().collection('users').get()
    return snapshot.docs.map(doc =>  doc.data());
  }
  async getAllResults(id) {
    const snapshot = await firebase.firestore().collection('users').doc(id).collection('result').get()
    return snapshot.docs.map(doc =>  doc.data());
  }
  async getResults() {
    const snapshot = await firebase.firestore().collection('users').doc().collection('result').get()
    return snapshot.docs.map(doc =>  doc.data());
  }

  searchUsers(searchValue) {
    return this.db.collection('users', ref => ref.where('nameToSearch', '>=', searchValue)
      .where('nameToSearch', '<=', searchValue + '\uf8ff'))
      .snapshotChanges();
  }

  searchUsersByAge(value) {
    return this.db.collection('users', ref => ref.orderBy('age').startAt(value)).snapshotChanges();
  }


  createUser(value, avatar) {
    return this.afAuth.auth.createUserWithEmailAndPassword(value.email, value.password).then(cred => {
      return this.db.collection('users').doc(cred.user.uid).set({
        displayName: value.displayName,
        nameToSearch: value.displayName.toLowerCase(),
        age: value.age,
        gender: value.gender,
        nationality: value.nationality,
        isAdmin: false,
        avatar
      });
    });
  }
  
}
