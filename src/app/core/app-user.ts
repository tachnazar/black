export interface AppUser {
  uid: string;
  email: string;
  displayName?: string;
  avatar?: string;
  age?: number;
  isAdmin?: boolean;
}
