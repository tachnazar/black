import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapComponent } from '../../pages/map/map.component';
import { NotificationsComponent } from '../../pages/notifications/notifications.component';
import { UserComponent } from '../../pages/user/user.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { TypographyComponent } from '../../pages/typography/typography.component';
import { RegisterComponent } from 'src/app/pages/register/register.component';
import { LoginComponent } from 'src/app/pages/login/login.component';
import { TestingComponent } from 'src/app/pages/testing/testing.component';
import { QuestionComponent } from 'src/app/pages/question/question.component';
import { ResultComponent } from 'src/app/pages/result/result.component';
import { MembersComponent } from 'src/app/pages/members/members.component';
import { AllResultComponent } from 'src/app/pages/all-result/all-result.component';
// import { RtlComponent } from "../../pages/rtl/rtl.component";

export const AdminLayoutRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'icons', component: IconsComponent },
  { path: 'maps', component: MapComponent },
  { path: 'notifications', component: NotificationsComponent },
  { path: 'user', component: UserComponent },
  { path: 'tables', component: TablesComponent },
  { path: 'typography', component: TypographyComponent },
  { path: 'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent},
  { path: 'testing', component: TestingComponent},
  { path: 'question', component: QuestionComponent},
  { path: 'result', component: ResultComponent},
  { path: 'members', component: MembersComponent},
  { path: 'all-results', component: AllResultComponent}

];
