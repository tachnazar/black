import { Component, OnInit } from "@angular/core";
import { Router, NavigationStart, Event, NavigationEnd } from '@angular/router';
@Component({
  selector: "app-admin-layout",
  templateUrl: "./admin-layout.component.html",
  styleUrls: ["./admin-layout.component.scss"]
})
export class AdminLayoutComponent implements OnInit {
  public sidebarColor: string = "red";
  timeout;
  routerChanged = true;
  constructor(private router: Router) {
    router.events.subscribe((event: Event) => {

    if (event instanceof NavigationStart) {
      // Show loading indicator
      this.routerChanged = true;
    }

    if (event instanceof NavigationEnd) {
      // Hide loading indicator
      this.timeout = setTimeout(() => {
        clearTimeout(this.timeout);
        this.routerChanged = false;
      }, 1000);
    }
  });}
  changeSidebarColor(color) {
    const sidebar = document.getElementsByClassName('sidebar')[0];
    const mainPanel = document.getElementsByClassName('main-panel')[0];

    this.sidebarColor = color;

    if (sidebar !== undefined) {
        sidebar.setAttribute('data', color);
    }
    if (mainPanel !== undefined) {
        mainPanel.setAttribute('data', color);
    }
  }
  changeDashboardColor(color) {
    const body = document.getElementsByTagName('body')[0];
    if (body && color === 'white-content') {
        body.classList.add(color);
    } else if (body.classList.contains('white-content')) {
      body.classList.remove('white-content');
    }
  }
  ngOnInit() {}
}
