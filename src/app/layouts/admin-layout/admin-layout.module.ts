import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AdminLayoutRoutes } from "./admin-layout.routing";
import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { IconsComponent } from "../../pages/icons/icons.component";
import { MapComponent } from "../../pages/map/map.component";
import { NotificationsComponent } from "../../pages/notifications/notifications.component";
import { UserComponent } from "../../pages/user/user.component";
import { TablesComponent } from "../../pages/tables/tables.component";
import { TypographyComponent } from "../../pages/typography/typography.component";
// import { RtlComponent } from "../../pages/rtl/rtl.component";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RegisterComponent } from 'src/app/pages/register/register.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentsModule } from 'src/app/components/components.module';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule, MatDialog, MatDialogModule, MatSelectModule, MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { AvatarDialogComponent } from 'src/app/pages/avatar-dialog/avatar-dialog.component';
import { LoginComponent } from 'src/app/pages/login/login.component';
import { TestingComponent } from 'src/app/pages/testing/testing.component';
// import {SlideshowModule} from 'ng-simple-slideshow';
import { SlideshowModule } from 'public_api';
import { QuestionComponent } from 'src/app/pages/question/question.component';
import { ResultComponent } from 'src/app/pages/result/result.component';
import { MembersComponent } from 'src/app/pages/members/members.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AllResultComponent } from 'src/app/pages/all-result/all-result.component';
import { AngularFireStorage } from '@angular/fire/storage';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    ComponentsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatDialogModule,
    SlideshowModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  declarations: [
    DashboardComponent,
    UserComponent,
    TablesComponent,
    IconsComponent,
    TypographyComponent,
    NotificationsComponent,
    MapComponent,
    RegisterComponent,
    LoginComponent,
    AvatarDialogComponent,
    TestingComponent,
    QuestionComponent,
    ResultComponent,
    MembersComponent,
    AllResultComponent
  ],
  entryComponents: [AvatarDialogComponent],
  exports: [
    UserComponent, RegisterComponent, AvatarDialogComponent, LoginComponent, TestingComponent, QuestionComponent, ResultComponent, MembersComponent, MatTableModule,MatPaginatorModule,MatSortModule, AllResultComponent
  ],
  providers:[
    TestingComponent,
    AngularFireStorage
  ]
})
export class AdminLayoutModule {}
