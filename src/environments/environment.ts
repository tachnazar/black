// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyA-CYTEPRbl_1SIs_OgPTPSb4Z0Ogt4Uxc",
    authDomain: "attentional-blink.firebaseapp.com",
    databaseURL: "https://attentional-blink.firebaseio.com",
    projectId: "attentional-blink",
    storageBucket: "attentional-blink.appspot.com",
    messagingSenderId: "491582912519",
    appId: "1:491582912519:web:2a2e4ae39d3a2cb6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
